package com.example.daytracker.Frontend.Quote

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.daytracker.Backend.model.QuoteModel
import com.example.daytracker.Backend.viewmodel.QuoteViewModel
import com.example.daytracker.R
import kotlinx.android.synthetic.main.fragment_add_quote.*
import kotlinx.android.synthetic.main.fragment_add_quote.view.*
import java.time.LocalDate


class AddQuoteFragment : Fragment() {

    private lateinit var quoteView: QuoteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add_quote, container, false)

        quoteView = ViewModelProvider(this).get(QuoteViewModel::class.java)

        view.add_quote_button.setOnClickListener {
            insertDataToDatabase()
        }
        view.brack_qoute_button.setOnClickListener {
            findNavController().navigate(R.id.action_addQuoteFragment_to_quoteFragment)
        }

        return view
    }

    private fun insertDataToDatabase() {
        val discription = qoutetextInputDescription.text.toString()
        val autor = addAutorInput.text.toString()
        val date = finddate()
        if (inputCheck(discription, autor)) {
            //Create User Object
            val quote = QuoteModel(0,autor, date, discription)
            //add Data to Database
            quoteView.addQuote(quote)
            Toast.makeText(requireContext(), "Gespeichert", Toast.LENGTH_SHORT).show()
            //Navigate back
            findNavController().navigate(R.id.action_addQuoteFragment_to_quoteFragment)
        } else {
            Toast.makeText(requireContext(), "Bitte fülle die Felder aus.", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun finddate(): String {
        val instanzdate = LocalDate.now()
        val date = instanzdate.dayOfMonth.toString() + "." + instanzdate.monthValue.toString() + "." + instanzdate.year.toString()

        return date

    }

    private fun inputCheck(too: String, one: String): Boolean {
        return !(TextUtils.isEmpty(too)  && TextUtils.isEmpty(one))
    }
}