package com.example.daytracker.Frontend.Adapter

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.daytracker.Backend.model.DayModel
import com.example.daytracker.Frontend.dayTraker.DayShowFragment
import com.example.daytracker.Frontend.dayTraker.DayShowFragmentDirections
import com.example.daytracker.R
import kotlinx.android.synthetic.main.item_day_list.view.*

class dayAdapter: RecyclerView.Adapter<dayAdapter.MyViewHolder>() {

    private var List = emptyList<DayModel>()
    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_day_list, parent, false))
    }

    override fun getItemCount(): Int {
        return List.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = List[position]
        holder.itemView.Day_book_time_data.text = currentItem.date
        holder.itemView.Day_book_daydiscript.text = currentItem.day_title
        holder.itemView.day_book_summery_discript.text = summery(currentItem.destript)


        holder.itemView.itemDayLayout.setOnClickListener {
            val action =
                DayShowFragmentDirections.actionDayShowFragmentToDayUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    private fun summery(destript: String): CharSequence? {
        var sum = destript
        if (sum.length > 50){
            sum = sum.substring(0, 50) + "..."
        }

        return sum

    }

    fun setData(day: List<DayModel>){
        this.List = day
        notifyDataSetChanged()
    }

}