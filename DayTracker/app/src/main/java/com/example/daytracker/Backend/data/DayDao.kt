package com.example.daytracker.Backend.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.daytracker.Backend.model.DayModel

@Dao
interface DayDao  {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addDay(pager: DayModel)

    @Update
    suspend fun updateDay(pager: DayModel)

    @Delete
    suspend fun deleteDay(pager: DayModel)

    @Query("SELECT * FROM day_pagers_table ORDER BY day_id ASC")
    fun readAllDayData(): LiveData<List<DayModel>>
}
