package com.example.daytracker.Backend.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName= "day_pagers_table")
data class DayModel(
    @PrimaryKey(autoGenerate = true)
    var day_id : Int,
    val day_title: String,
    var date: String,
    var destript: String): Parcelable