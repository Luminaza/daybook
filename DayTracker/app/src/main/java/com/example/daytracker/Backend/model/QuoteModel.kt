package com.example.daytracker.Backend.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName= "qouts_table")
class QuoteModel (
    @PrimaryKey(autoGenerate = true)
    var qoute_id : Int,
    val name: String,
    var date: String,
    var destript: String): Parcelable