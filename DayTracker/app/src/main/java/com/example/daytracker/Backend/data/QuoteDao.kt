package com.example.daytracker.Backend.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.daytracker.Backend.model.QuoteModel

@Dao
interface QuoteDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addQuote(pager: com.example.daytracker.Backend.model.QuoteModel)

    @Update
    suspend fun updateQuote(pager: com.example.daytracker.Backend.model.QuoteModel)

    @Delete
    suspend fun deleteQuote(pager: com.example.daytracker.Backend.model.QuoteModel)

    @Query("SELECT * FROM qouts_table ORDER BY qoute_id ASC")
    fun readAllQuoteData(): LiveData<List<QuoteModel>>
}