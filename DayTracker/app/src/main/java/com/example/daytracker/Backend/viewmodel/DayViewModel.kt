package com.example.daytracker.Backend.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.daytracker.Backend.data.MyDatabase
import com.example.daytracker.Backend.model.DayModel
import com.example.daytracker.Backend.reposetory.DayReposetory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DayViewModel (application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<DayModel>>
    private val repository: DayReposetory

    init{
        val dayDao = MyDatabase.getDatabase(application).DayDao()
        repository = DayReposetory(dayDao)
        readAllData = repository.readAllData
    }

    fun addDay(day: DayModel){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addDay(day)
        }
    }

    fun updateDay(day:DayModel){
        viewModelScope.launch(Dispatchers.IO){
            repository.updateDay(day)
        }
    }

    fun deleteDay(day:DayModel){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteDay(day)
        }
    }


}