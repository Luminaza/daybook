package com.example.daytracker.Frontend.dayTraker

import android.app.AlertDialog
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.daytracker.Backend.model.DayModel
import com.example.daytracker.Backend.viewmodel.DayViewModel
import com.example.daytracker.R
import kotlinx.android.synthetic.main.fragment_add_day.*
import kotlinx.android.synthetic.main.fragment_day_update.*
import kotlinx.android.synthetic.main.fragment_day_update.view.*


class DayUpdateFragment : Fragment() {

    private val args by navArgs<DayUpdateFragmentArgs>()
    private lateinit var model: DayViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      val view = inflater.inflate(R.layout.fragment_day_update, container, false)

        model = ViewModelProvider(this).get(DayViewModel::class.java)

        view.upTitelInput.setText(args.dayUpgrade.day_title)
        view.updaytextInputDescription.setText(args.dayUpgrade.destript)
        view.upDate.text = args.dayUpgrade.date


        view.up_day_button.setOnClickListener{

            updateItem()
        }
        view.brack_up_button.setOnClickListener{
            findNavController().navigate(R.id.action_dayUpdateFragment_to_dayShowFragment)
        }

        view.delet_up_button.setOnClickListener {
            Itemdelete()
        }


    return view
    }

    private fun Itemdelete() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Ja"){_, _ ->
            model.deleteDay(args.dayUpgrade)
            findNavController().navigate(R.id.action_dayUpdateFragment_to_dayShowFragment)
        }
        builder.setNegativeButton("Nein"){_, _ ->
        }
        builder.setTitle("Delete ${args.dayUpgrade.day_title}")
        builder.setMessage("Bis du dir sicher das du den Eintrag Löschen willst?( ${args.dayUpgrade.day_title})")
        builder.create().show()
    }

    private fun updateItem() {

        val discription = updaytextInputDescription.text.toString()
        val title = upTitelInput.text.toString()
        val date = upDate.text.toString()
        val id = args.dayUpgrade.day_id

        if (inputCheck(discription, title, date)) {
            //Create User Object
            val new = DayModel(id ,title, date, discription)
            //add Data to Database
            model.updateDay(new)
            Toast.makeText(requireContext(), "funktioniert", Toast.LENGTH_SHORT).show()
            //Navigate back

            findNavController().navigate(R.id.action_dayUpdateFragment_to_dayShowFragment)
        } else {
            Toast.makeText(requireContext(), "Please fill out all fields.", Toast.LENGTH_SHORT)
                .show()

        }
    }

private fun inputCheck(too: String, date: String, one: String): Boolean {
    return !(TextUtils.isEmpty(too) && TextUtils.isEmpty(date) && TextUtils.isEmpty(one))
}

}