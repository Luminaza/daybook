package com.example.daytracker.Frontend.dayTraker


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.daytracker.Backend.viewmodel.DayViewModel
import com.example.daytracker.Frontend.Adapter.dayAdapter
import com.example.daytracker.R
import kotlinx.android.synthetic.main.fragment_day_show.view.*


class DayShowFragment : Fragment() {

    private lateinit var ViewModel: DayViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_day_show, container, false)



        // Recyclerview
        val adapter = dayAdapter()
        val recyclerView = view.dayrecyclerview
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // UserViewModel
        ViewModel = ViewModelProvider(this).get(DayViewModel::class.java)
        ViewModel.readAllData.observe(viewLifecycleOwner, Observer { day ->
            adapter.setData(day)
        })


        view.add_botton.setOnClickListener{
            findNavController().navigate(R.id.action_dayShowFragment_to_addDayFragment)
        }

        view.day_nav_quote.setOnClickListener{
                findNavController().navigate(R.id.action_dayShowFragment_to_quoteFragment)
        }

        view.day_nav_food.setOnClickListener{
                 findNavController().navigate(R.id.action_dayShowFragment_to_htmlFragment)
        }



        return view
    }



}