package com.example.daytracker.Frontend.Quote

import android.app.AlertDialog
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.daytracker.Backend.model.QuoteModel
import com.example.daytracker.Backend.viewmodel.QuoteViewModel
import com.example.daytracker.R
import kotlinx.android.synthetic.main.fragment_qoute_update.*
import kotlinx.android.synthetic.main.fragment_qoute_update.view.*


class QouteUpdateFragment : Fragment() {

    private val args by navArgs<QouteUpdateFragmentArgs>()
    private lateinit var model: QuoteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_qoute_update, container, false)

        model = ViewModelProvider(this).get(QuoteViewModel::class.java)

        view.upAutorInput.setText(args.upQuote.name)
        view.qouteNumber.text = args.upQuote.qoute_id.toString()
        view.upQouteDate.text = args.upQuote.date
        view.upQoutetextInputDescription.setText(args.upQuote.destript)

        view.up_da_qoute_button.setOnClickListener{
            updateItem()
        }
        view.brack_qoute_button.setOnClickListener{
            findNavController().navigate(R.id.action_qouteUpdateFragment_to_quoteFragment)
        }

        view.delet_qoute_button.setOnClickListener {
            Itemdelete()
        }

        return view
    }

    private fun updateItem() {
        val discription = upQoutetextInputDescription.text.toString()
        val autor = upAutorInput.text.toString()
        val date = upQouteDate.text.toString()
        val id = args.upQuote.qoute_id
        if (inputCheck(discription, autor, date)) {
            //Create User Object
            val qout = QuoteModel(id,autor, date, discription)
            //add Data to Database
            model.updateQuote(qout)
            Toast.makeText(requireContext(), "YEAH", Toast.LENGTH_SHORT).show()
            //Navigate back
            findNavController().navigate(R.id.action_qouteUpdateFragment_to_quoteFragment)
            Toast.makeText(requireContext(), "Please fill out all fields.", Toast.LENGTH_SHORT)
                .show()

        }
    }
    private fun inputCheck(too: String, date: String, one: String): Boolean {
        return !(TextUtils.isEmpty(too) && TextUtils.isEmpty(date) && TextUtils.isEmpty(one))
    }
    private fun Itemdelete() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Ja"){_, _ ->
            model.deleteQuote(args.upQuote)
            findNavController().navigate(R.id.action_qouteUpdateFragment_to_quoteFragment)
        }
        builder.setNegativeButton("Nein"){_, _ ->
        }
        builder.setTitle("Delete")
        builder.setMessage("Bis du dir sicher das du den Eintrag Löschen willst?")
        builder.create().show()
    }

}