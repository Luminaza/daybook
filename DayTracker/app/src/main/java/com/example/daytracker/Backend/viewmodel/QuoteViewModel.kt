package com.example.daytracker.Backend.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.daytracker.Backend.data.MyDatabase
import com.example.daytracker.Backend.model.QuoteModel
import com.example.daytracker.Backend.reposetory.QuoteReposetory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class QuoteViewModel (application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<QuoteModel>>
    private val repository: QuoteReposetory

    init {
        val qouteDao = MyDatabase.getDatabase(application).QuoteDao()
        repository = QuoteReposetory(qouteDao)
        readAllData = repository.readAllData
    }
    fun addQuote(info: QuoteModel){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addDay(info)
        }
    }

    fun updateQuote(info: QuoteModel){
        viewModelScope.launch(Dispatchers.IO){
            repository.updateDay(info)
        }
    }

    fun deleteQuote(info: QuoteModel){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteDay(info)
        }
    }

}
