package com.example.daytracker.Frontend.html

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.navigation.fragment.findNavController
import com.example.daytracker.R
import kotlinx.android.synthetic.main.fragment_day_show.view.*
import kotlinx.android.synthetic.main.fragment_html.*
import kotlinx.android.synthetic.main.fragment_html.view.*

class htmlFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_html, container, false)


        view.html_nav_jornal.setOnClickListener {
            findNavController().navigate(R.id.action_htmlFragment_to_dayShowFragment)
        }

        view.html_nav_quote.setOnClickListener {
            findNavController().navigate(R.id.action_htmlFragment_to_quoteFragment)
        }


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val myWebView: WebView = view.findViewById(R.id.webview)
        myWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                url: String
            ): Boolean {
                view.loadUrl(url)
                return true
            }
        }

        myWebView.loadUrl("https://www.brigitte.de/horoskop/tageshoroskop/")
        myWebView.settings.javaScriptEnabled = true
        myWebView.settings.allowContentAccess = true
        myWebView.settings.domStorageEnabled = true
        myWebView.settings.useWideViewPort = true
    }

}
