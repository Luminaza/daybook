package com.example.daytracker.Backend.reposetory

import androidx.lifecycle.LiveData
import com.example.daytracker.Backend.data.QuoteDao
import com.example.daytracker.Backend.model.QuoteModel

class QuoteReposetory (private val qoute : QuoteDao) {
    val readAllData: LiveData<List<QuoteModel>> = qoute.readAllQuoteData()

    suspend fun addDay(pages: QuoteModel){
        qoute.addQuote(pages)
    }

    suspend fun updateDay(pages: QuoteModel){
        qoute.updateQuote(pages)
    }

    suspend fun deleteDay(pages: QuoteModel){
        qoute.deleteQuote(pages)
    }
}