package com.example.daytracker.Frontend.Quote

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.daytracker.Backend.viewmodel.QuoteViewModel
import com.example.daytracker.Frontend.Adapter.QuoteAdapter
import com.example.daytracker.R
import kotlinx.android.synthetic.main.fragment_quote.view.*


class QuoteFragment : Fragment() {

    private lateinit var Model: QuoteViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_quote, container, false)

        view.qout_nav_day.setOnClickListener {
            findNavController().navigate(R.id.action_quoteFragment_to_dayShowFragment)
        }
        view.qout_nav_html.setOnClickListener {
            findNavController().navigate((R.id.action_quoteFragment_to_htmlFragment))
        }

        // Recyclerview
        val adapter = QuoteAdapter()
        val recyclerView = view.quoterecyclerview
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // ViewModel
        Model = ViewModelProvider(this).get(QuoteViewModel::class.java)
      Model.readAllData.observe(viewLifecycleOwner, Observer { data ->
          adapter.setData(data)
        })

        view.add_quot_botton.setOnClickListener{
            findNavController().navigate(R.id.action_quoteFragment_to_addQuoteFragment)
        }


        return view
    }


}