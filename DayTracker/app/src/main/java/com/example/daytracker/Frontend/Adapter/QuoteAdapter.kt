package com.example.daytracker.Frontend.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.daytracker.Backend.model.QuoteModel
import com.example.daytracker.Frontend.Quote.QuoteFragmentDirections
import com.example.daytracker.R
import kotlinx.android.synthetic.main.item_quote.view.*

class QuoteAdapter : RecyclerView.Adapter<QuoteAdapter.MyViewHolder>() {

    private var List = emptyList<QuoteModel>()
    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_quote, parent, false))
    }

    override fun getItemCount(): Int {
        return List.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = List[position]
        holder.itemView.show_qoute.text = currentItem.destript
        holder.itemView.show_qoute_date.text = currentItem.date
        holder.itemView.show_qoute_aotor.text = currentItem.name

        holder.itemView.item_qoute_Layout.setOnClickListener{
           val action = QuoteFragmentDirections.actionQuoteFragmentToQouteUpdateFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }
    }

    fun setData(data: List<QuoteModel>){
        this.List = data
        notifyDataSetChanged()
    }

}