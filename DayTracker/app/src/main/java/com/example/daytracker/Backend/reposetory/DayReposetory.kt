package com.example.daytracker.Backend.reposetory

import androidx.lifecycle.LiveData
import com.example.daytracker.Backend.data.DayDao
import com.example.daytracker.Backend.model.DayModel

class DayReposetory (private val dayDao: DayDao) {

    val readAllData: LiveData<List<DayModel>> = dayDao.readAllDayData()

    suspend fun addDay(pages:DayModel){
        dayDao.addDay(pages)
    }

    suspend fun updateDay(pages:DayModel){
        dayDao.updateDay(pages)
    }

    suspend fun deleteDay(pages:DayModel){
        dayDao.deleteDay(pages)
    }


}