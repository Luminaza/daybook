package com.example.daytracker.Frontend.dayTraker

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.daytracker.Backend.model.DayModel
import com.example.daytracker.Backend.viewmodel.DayViewModel
import com.example.daytracker.R
import kotlinx.android.synthetic.main.fragment_add_day.*
import kotlinx.android.synthetic.main.fragment_add_day.view.*
import kotlinx.android.synthetic.main.fragment_day_show.view.*
import java.time.LocalDate


class AddDayFragment : Fragment() {

    private lateinit var dayView: DayViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_add_day, container, false)
        dayView = ViewModelProvider(this).get(DayViewModel::class.java)

        view.add_day_button.setOnClickListener {
            insertDataToDatabase()
        }
        view.brack_day_button.setOnClickListener {
            findNavController().navigate(R.id.action_addDayFragment_to_dayShowFragment)
        }
        return view
    }

    private fun insertDataToDatabase() {
        val discription = daytextInputDescription.text.toString()
        val title = addTitelInput.text.toString()
        val date = finddate()
        if (inputCheck(discription, title, date)) {
            //Create User Object
            val day = DayModel(0,title, date, discription)
            //add Data to Database
            dayView.addDay(day)
            Toast.makeText(requireContext(), "gesichert ^_^", Toast.LENGTH_SHORT).show()
            //Navigate back
            findNavController().navigate(R.id.action_addDayFragment_to_dayShowFragment)
        } else {
            Toast.makeText(requireContext(), "Bitte Fühle die felder aus", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun finddate(): String {
        val instanzdate = LocalDate.now()
        val date = instanzdate.dayOfMonth.toString() + "." + instanzdate.monthValue.toString() + "." + instanzdate.year.toString()

        return date

    }

    private fun inputCheck(too: String, date: String, one: String): Boolean {
        return !(TextUtils.isEmpty(too) && TextUtils.isEmpty(date) && TextUtils.isEmpty(one))
    }
}